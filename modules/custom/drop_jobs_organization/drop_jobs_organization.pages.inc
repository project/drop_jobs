<?php
/**
 * @file
 * Drop Jobs Organization pages.
 */

/**
 * Page callback.
 *
 * Dislays organization dashboard.
 *
 * @see drop_jobs_organization_menu()
 * @see drop_jobs_core_menu_table()
 */
function drop_jobs_organization_dashboard() {
  $cols_per_row = variable_get_value('drop_jobs_dashboard_user_cols');

  $classes = array(
    'dashboard/organization/page' => 'organization',
  );

  $exclude = array(
    'dashboard/organization',
    'user/logout',
  );

  return drop_jobs_core_menu_table('menu-drop-jobs-dashboard', $cols_per_row, NULL, $classes, $exclude, TRUE);
}

/**
 * Form callback.
 *
 * Displays organization dashboard settings form.
 *
 * @see drop_jobs_organization_menu()
 */
function drop_jobs_organization_preferences($form, $form_state) {
  $form['wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global Preferences'),
    '#collapsible' => FALSE,
  );

  return $form;
}
