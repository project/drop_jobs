<?php
/**
 * @file
 * drop_jobs_organization.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function drop_jobs_organization_default_rules_configuration() {
  $items = array();
  $items['rules_register_as_organization'] = entity_import('rules_config', '{ "rules_register_as_organization" : {
      "LABEL" : "Register as Organization",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "-5",
      "TAGS" : [ "drop_jobs_organization" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "user_insert" ],
      "IF" : [
        { "data_is" : {
            "data" : [ "site:current-page:path" ],
            "value" : "user\\/register\\/organization"
          }
        }
      ],
      "DO" : [
        { "user_add_role" : {
            "account" : [ "account" ],
            "roles" : { "value" : { "196496503" : "196496503" } }
          }
        }
      ]
    }
  }');
  return $items;
}
